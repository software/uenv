uenv
====

| *uenv* - as in **u**\ seful **env**\ ironments - is a collection of scripts,
 |systemd| |systemd.service| and |systemd.timer| units and configuration, that
 go along with it.
| It is meant to help organizing your workflow and hiding away user and system
 services in neat little separate environments.
| Arguably a couple of the |systemd.unit| might only be useful, when using
 window managers like |awesome|, that stem from a minimalistic approach.
| Some of the functionalities implemented here can be found in desktop
 environments like |kde| or |gnome|.
| *uenv* and all of its components are licensed under the GPLv3.

Separate tmux environments
--------------------------
| |tmux| is a very useful tool to multiplex your terminals. It offers tiling
 and a high configurability.
| Here it is used for a couple of services to start an environment in which
 other |cli| programs can run.
| In those services the following |systemd.exec| is set:

  .. code:: ini

    Environment=TMUX_TMPDIR=%t

| This will store the |tmux| server socket in the users' *XDG_RUNTIME_DIR*.
| Therefore it will be necessary to set your *TMUX_TMPDIR* |environment_variable|
 for your |bash| or |zsh| (or whatever you run) to the following:

  .. code:: bash

    export TMUX_TMPDIR="$XDG_RUNTIME_DIR/"

Afterwards a simple

  .. code:: bash

    tmux -L <name-of-socket> attach

| will attach you to that separate environment.
| **Note**: All of these separate user service environments are dependant on
 your login. If there is no login, |systemd| will kill all of your user
 services! You can get around this by using |loginctl| to enable lingering for
 your user.

  .. code:: bash

    loginctl enable-linger


Services
--------

compton --user
______________
| The |compton| user service needs a properly exported *$DISPLAY* variable to
 work. You can set this in your |systemd-user.conf|:

  .. code:: ini

    DefaultEnvironment=DISPLAY=:0

| The compton user service is anti-dependent on the realtime kernel (i.e. it will
 not start, if **/sys/kernel/realtime** is present and not empty).
| You can start and enable it like any other systemd user service:

  .. code:: bash

     systemctl --user start compton
     systemctl --user enable compton

conky@ --user
______________
| The |conky| user service needs a properly exported *$DISPLAY* variable to work.
| You can set this in your |systemd-user.conf|:

  .. code:: ini

    DefaultEnvironment=DISPLAY=:0

| The |conky| user service starts conky scripts by name. This means, the word
 after the *@* directly relates to configuration files in your conky
 configuration directory (*~/.config/conky/*).
| You can start and enable it like any other systemd user service, by supplying
 it with a configuration file name:

  .. code:: bash

     systemctl --user start conky@my-configuration-name
     systemctl --user enable conky@my-configuration-name

cpupower-rt
___________
| The cpupower package on |arch_linux| has been |cpupower_feature_request|, to
 allow easier appliance of CPU settings based on other profiles, than the
 system's default.
| This is very useful, when using a real-time kernel: cpupower-rt settings will
 be applied **after** the general cpupower settings have been applied.
| The *cpupower-rt.service* is dependant on a realtime kernel. It will only
 start, if **/sys/kernel/realtime** is present and not empty.
| You can start and enable it like any other systemd service:

  .. code:: bash

    systemctl start cpupower-rt
    systemctl enable cpupower-rt

irssi --user
____________
| The systemd user service *irssi.service* starts the |irc| client |irssi| in a
 separate |tmux| environment for the current user.
| To connect to it after starting the service, just do

  .. code:: bash

    tmux -L irssi attach

monitoring --user
_________________
| The systemd user service *monitoring.service* is just a useful tool for
 monitoring.
| Currently it starts |htop|, |glances| and asks loginctl for the current user
 processes in a separate |tmux| environment.
| To connect to it after starting the service, just do

  .. code:: bash

    tmux -L mon attach

mpd@ --user
___________
| The specialized systemd user service *mpd@.service* starts |mpd| with a
 separate configuration located in *~/.config/mpd/mpd-<name-of-server>.conf*. It
 will also use |pax11publish| to connect pulseaudio to that server, if the
 configuration is not called localhost* or like your hostname*!
| Start and enable it like any other systemd user service:

  .. code:: bash

    systemctl --user start mpd@myserver
    systemctl --user enable mpd@myserver

postpone-screensaver --user
___________________________
| With the systemd user service and timer *postpone-screensaver.{service,timer}*
 one can - as the name implies - postpone one's screensaver from blanking the
 screen. The script checks for programs set in *~/.config/postpone-screensaver*
 and can be started and activated like any other timer/service:

  .. code:: bash

    systemctl --user start postpone-screensaver.timer 
    systemctl --user enable postpone-screensaver.timer 

rtorrent --user
_______________
| rtorrent doesn't really come with any systemd service. Here it is started
 within a separate |tmux| environment. It will automatically create
 *~/Downloads/rtorrent/{tmp,session}*, if non-existent and set its working
 directory to *~/Downloads*.
| The systemd user service is started/ enabled like this:

  .. code:: bash

    systemctl --user start rtorrent
    systemctl --user enable rtorrent

rtorrent@
_________
| The systemd system service rtorrent@.service is quite similar to the user
 service rtorrent.service in functionality, but on top offers the possibility of
 dependency to a system service - like |openvpn| (the default being a profile
 called *secure*).
| **Note:** In a separate configuration file (*"/etc/conf.d/rtorrent@.conf"*),
 the *TMUX_TMPDIR* should be set, as it otherwise defaults to using */tmp/*!
| The system service can be started and enabled like this:

  .. code:: bash

    systemctl start rtorrent@<your-user-name>
    systemctl enable rtorrent@<your-user-name>

| To connect to the user and system service after starting the service, just do

  .. code:: bash

    tmux -L rt attach

ssh-agent --user
________________
| The systemd user service *ssh-agent.service* starts a ssh-agent for the
 current user, which makes it independent from the current login session.
| In its |systemd.exec| *SSH_AUTH_SOCK* will be set to the user's
 *XDG_RUNTIME_DIR*.
| Therefore make sure to set your *SSH_AUTH_SOCK* |environment_variable| for
 your |bash| or |zsh| (or whatever you run) to the following:

  .. code:: bash

    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

| Otherwise you can start and stop the service like any other:

  .. code:: bash

    systemctl --user start ssh-agent
    systemctl --user enable ssh-agent


syndaemon --user
________________
| The |xf86-input-synaptics| package comes with */usr/bin/syndaemon*. This tool
 is used to disable the touchpad while typing. While desktop environments like
 |kde| and |gnome| use it integrated, it can also be started as a systemd user
 service:

  .. code:: bash

    systemctl --user start syndaemon
    systemctl --user enable syndaemon

systemd-analyze-plot
____________________
| Systemd comes with a functionality to plot the system's boot up process in
 svg and other file formats. This user service uses */usr/bin/systemd-analyze*
 to plot a svg to a predefined directory. Configuration takes place in
 *~/.config/systemd/plot.conf*.
| To start/enable the service afterwards just do:  

  .. code:: bash

    systemctl --user start systemd-analyze-plot
    systemctl --user enable systemd-analyze-plot

tmux --user
___________
| The systemd user service *tmux.service* starts a separate |tmux| environment.
| To start/enable it, just do:

  .. code:: bash

    systemctl --user start tmux
    systemctl --user enable tmux

| To connect to the user service after starting it, just do

  .. code:: bash

    tmux attach

update-motd
___________
| The system service *update-motd.service* updates your MOTD (message of the day)
 with your hostname, your currently running kernel version, available
 package updates and needed security updates.
| To be able to check pacman and security updates, the accompanying
 *update-motd.timer* will start the service hourly.
| If a /etc/motd.name is available, then that file will be used instead of
 /etc/hostname to generate a banner.
| For the security updates |aur-arch-audit| is needed and if you want to have
 colorful output of it all, install |aur-python-lolcat|.
| To start/enable the timer just do:

  .. code:: bash

    systemctl --system start update-motd
    systemctl --system enable update-motd

| Thanks to |website-xen| for the inspiration!

weechat --user
______________
| The systemd user service *weechat.service* starts the |irc| client |weechat| in
 a separate |tmux| environment for the current user.
| To start/enable it, just do:

  .. code:: bash

    systemctl --user start weechat
    systemctl --user enable weechat

| To connect to it after starting the service, just do

  .. code:: bash

    tmux -L weechat attach


.. |systemd| raw:: html

  <a href="https://en.wikipedia.org/wiki/Systemd" target="_blank">systemd</a>

.. |awesome| raw:: html

  <a href="http://awesome.naquadah.org/" target="_blank">awesome</a>

.. |gnome| raw:: html

  <a href="https://gnome.org" target="_blank">Gnome</a>

.. |kde| raw:: html

  <a href="https://kde.org" target="_blank">KDE</a>

.. |systemd.unit| raw:: html

  <a href="http://www.freedesktop.org/software/systemd/man/systemd.unit.html"
  target="_blank">units</a>

.. |systemd.service| raw:: html

  <a
  href="http://www.freedesktop.org/software/systemd/man/systemd.service.html"
  target="_blank">service</a>

.. |systemd.timer| raw:: html

  <a href="http://www.freedesktop.org/software/systemd/man/systemd.timer.html"
  target="_blank">timer</a>

.. |systemd.exec| raw:: html

  <a href="http://www.freedesktop.org/software/systemd/man/systemd.exec.html"
  target="_blank">Environment</a>

.. |tmux| raw:: html

  <a href="https://tmux.github.io/" target="_blank">tmux</a>

.. |cli| raw:: html

  <a href="https://en.wikipedia.org/wiki/Command-line_interface"
  target="_blank">cli</a>

.. |environment_variable| raw:: html

  <a href="https://en.wikipedia.org/wiki/Environment_variable"
  target="_blank">environment variable</a>

.. |bash| raw:: html

  <a href="https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29"
  target="_blank">bash</a>

.. |zsh| raw:: html

  <a href="https://en.wikipedia.org/wiki/Z_shell" target="_blank">zsh</a>

.. |systemd-user.conf| raw:: html

  <a
  href="http://www.freedesktop.org/software/systemd/man/systemd-user.conf.html"
  target="_blank">/etc/systemd/user.conf</a>

.. |compton| raw:: html

  <a href="https://github.com/chjj/compton/" target="_blank">compton</a>

.. |conky| raw:: html

  <a href="https://github.com/brndnmtthws/conky" target="_blank">conky</a>

.. |real-time_kernel| raw:: html

  <a href="https://www.kernel.org/pub/linux/kernel/projects/rt/"
  target="_blank">real-time kernel</a>

.. |kernel_parameter| raw:: html

  <a href="https://www.kernel.org/doc/Documentation/kernel-parameters.txt"
  target="_blank">kernel parameter</a>

.. |grub| raw:: html

  <a href="https://wiki.archlinux.org/index.php/Kernel_parameters#GRUB"
  target="_blank">GRUB</a>

.. |syslinux| raw:: html

  <a href="https://wiki.archlinux.org/index.php/Kernel_parameters#Syslinux"
  target="_blank">syslinux</a>

.. |systemd-boot| raw:: html

  <a href="https://wiki.archlinux.org/index.php/Kernel_parameters#systemd-boot"
  target="_blank">systemd-boot</a>

.. |arch_linux| raw:: html

  <a href="https://www.archlinux.org/" target="_blank">Arch Linux</a>

.. |htop| raw:: html

  <a href="https://en.wikipedia.org/wiki/Htop" target="_blank">htop</a>

.. |irc| raw:: html

  <a href="https://en.wikipedia.org/wiki/Internet_Relay_Chat"
  target="_blank">IRC</a>

.. |irssi| raw:: html

  <a href="http://irssi.org/" target="_blank">Irssi</a>

.. |glances| raw:: html

  <a href="https://github.com/nicolargo/glances" target="_blank">glances</a>

.. |openvpn| raw:: html

  <a href="https://github.com/nicolargo/glances" target="_blank">OpenVPN</a>

.. |mpd| raw:: html

  <a href="http://www.musicpd.org/" target="_blank">MPD</a>

.. |pax11publish| raw:: html

  <a href="http://linux.die.net/man/1/pax11publish"
  target="_blank">pax11publish</a>

.. |xf86-input-synaptics| raw:: html

  <a
  href="https://www.archlinux.org/packages/extra/x86_64/xf86-input-synaptics/"
  target="_blank">xf86-input-synaptics</a>

.. |loginctl| raw:: html

  <a href="http://www.freedesktop.org/software/systemd/man/loginctl.html"
  target="_blank">loginctl</a>

.. |pacman| raw:: html

  <a href="https://en.wikipedia.org/wiki/Arch_Linux#Pacman"
  target="_blank">pacman</a>

.. |weechat| raw:: html

  <a href="http://weechat.org/" target="_blank">weechat</a>

.. |cpupower_feature_request| raw:: html

  <a href="https://bugs.archlinux.org/task/44270" target="_blank">changed</a>

.. |aur-arch-audit| raw:: html

  <a href="https://aur.archlinux.org/packages/arch-audit/"
  target="_blank">arch-audit</a>

.. |aur-python-lolcat| raw:: html

  <a href="https://aur.archlinux.org/packages/python-lolcat/"
  target="_blank">python-lolcat</a>

.. |website-xen| raw:: html

  <a href="http://xengi.de" target="_blank">XenGi</a>


