#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

host="$(hostname)"

function connect ()
{
  if [[ "$1" != "${host}"* ]] && [[ "$1" != localhost* ]]; then
    echo "Connecting with host \"$1\" using pax11publish."
    pax11publish -S "$1" -e
  else
    exit 0
  fi
}

function disconnect ()
{
  if [[ $1 != "${host}"* ]] && [[ $1 != localhost* ]]; then
    echo "Disconnecting from host \"$1\" using pax11publish."
    pax11publish -r
  else
    exit 0
  fi
}

function print_help ()
{
  echo "Use this script as follows:"
  echo "Connect to host with mpd-pulse -c hostname"
  echo "Disconnect from host with mpd-pulse -d hostname"
  echo "If your given hostname is localhost or matches your local machine, no action will be taken."
}

if [ ${#@} -gt 0 ]; then
  while getopts 'c:d:h' flag; do
    case "${flag}" in
      c)
        if [ -n "${OPTARG}" ]; then
          connect "${OPTARG}"
        else
          exit 1
        fi
        ;;
      d)
        if [ -n "${OPTARG}" ]; then
          disconnect "${OPTARG}"
        else
          exit 1
        fi
        ;;
      h)
        print_help
        ;;
      *)
        echo "Error. Unrecognized option: ${flag}."
        exit 1
        ;;
    esac
  done
else
  print_help
fi

exit 0
